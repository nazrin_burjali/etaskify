package com.iba.eTaskify.service;
import com.iba.eTaskify.dto.RegistrationDto;
import com.iba.eTaskify.entity.RegistrationForm;
import com.iba.eTaskify.repository.RegistrationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class RegistrationService {

    @Autowired
    private RegistrationRepository registrationRepository;


    @Autowired
    private PasswordService passwordRepository;


    private static RegistrationDto convertEntityToDto(RegistrationForm registrationForm) {
        RegistrationDto dto = new RegistrationDto();
        dto.setOrganizationName(registrationForm.getOrganizationName());
        dto.setPhoneNumber(registrationForm.getPhoneNumber());
        dto.setUsername(registrationForm.getUsername());
        dto.setEmail(registrationForm.getEmail());

        return dto;
    }

    @Transactional
    public RegistrationDto addRegistrationForm(RegistrationForm form) {
        String hashedPassword= passwordRepository.hashPassword(form.getPassword());
        form.setPassword(hashedPassword);
        return convertEntityToDto(registrationRepository.save(form));
    }

    public RegistrationDto getRegistrationFormById(int id) {
        Optional<RegistrationForm> optional = registrationRepository.findById(id);
        RegistrationDto registrationDto = null;
        if (optional.isPresent()) {
            registrationDto = convertEntityToDto(optional.get());
        } else {
            throw new RuntimeException("error while get by id");
        }

        return registrationDto;
    }


    public List<RegistrationDto> getAll() {
        List<RegistrationDto> forms = registrationRepository.findAll()
        .stream()
                .map(RegistrationService::convertEntityToDto)
                .collect(Collectors.toList());
        return forms;
    }

    public RegistrationDto updateRegistrationForm(int id,RegistrationDto registrationDto) {
        Optional<RegistrationForm> optional = registrationRepository.findById(id);
        if (optional.isPresent()){
            RegistrationForm form = new RegistrationForm();
            form.setOrganizationName(registrationDto.getOrganizationName());
            form.setPhoneNumber(registrationDto.getPhoneNumber());
            form.setUsername(registrationDto.getUsername());
            form.setEmail(registrationDto.getEmail());
            return convertEntityToDto(registrationRepository.save(form));
        }
        else {
            throw new RuntimeException("error while update");
        }
    }

    public void deleteRegistrationForm(int id) {
        registrationRepository.deleteById(id);
    }
}
