package com.iba.eTaskify.service;
import com.iba.eTaskify.dto.RegistrationDto;
import com.iba.eTaskify.dto.UserDto;
import com.iba.eTaskify.entity.RegistrationForm;
import com.iba.eTaskify.entity.Role;
import com.iba.eTaskify.entity.User;
import com.iba.eTaskify.repository.RegistrationRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class UserService {

    @Autowired
    private RegistrationRepository registrationRepository;

    @Autowired
    private PasswordService passwordRepository;


    private static RegistrationDto convertEntityToDto(RegistrationForm registrationForm) {
        RegistrationDto dto = new RegistrationDto();
        dto.setOrganizationName(registrationForm.getOrganizationName());
        dto.setPhoneNumber(registrationForm.getPhoneNumber());
        dto.setUsername(registrationForm.getUsername());
        dto.setEmail(registrationForm.getEmail());
        return dto;
    }

    public RegistrationDto addUser(User user){
        RegistrationForm form = new RegistrationForm();
        form.setName(user.getName());
        form.setSurname(user.getSurname());
        form.setEmail(user.getEmail());
        String hashedPassword= passwordRepository.hashPassword(user.getPassword());
        form.setPassword(hashedPassword);
        form.setRole(Role.USER);
        return convertEntityToDto(registrationRepository.save(form));
    }



    }


