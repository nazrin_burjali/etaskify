package com.iba.eTaskify.service;

import com.iba.eTaskify.dto.RegistrationDto;
import com.iba.eTaskify.entity.OrgAdmin;
import com.iba.eTaskify.entity.RegistrationForm;
import com.iba.eTaskify.entity.Role;
import com.iba.eTaskify.repository.RegistrationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrgAdminService {
    @Autowired
    private RegistrationRepository registrationRepository;

    @Autowired
    private PasswordService passwordRepository;

    private static RegistrationDto convertEntityToDto(RegistrationForm registrationForm) {
        RegistrationDto dto = new RegistrationDto();
        dto.setOrganizationName(registrationForm.getOrganizationName());
        dto.setPhoneNumber(registrationForm.getPhoneNumber());
        dto.setUsername(registrationForm.getUsername());
        dto.setEmail(registrationForm.getEmail());
        return dto;
    }


    public RegistrationDto addAdmin(OrgAdmin admin){
            RegistrationForm form = new RegistrationForm();
            form.setOrganizationName(admin.getOrganizationName());
            form.setPhoneNumber(admin.getPhoneNumber());
            form.setAddress(admin.getAddress());
            form.setUsername(admin.getUsername());
            form.setEmail(admin.getEmail());
            String hashedPassword= passwordRepository.hashPassword(admin.getPassword());
            form.setPassword(hashedPassword);
            form.setRole(Role.ADMIN);
            return convertEntityToDto(registrationRepository.save(form));
        }

}

