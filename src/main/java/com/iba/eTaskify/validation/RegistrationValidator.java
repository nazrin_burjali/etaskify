package com.iba.eTaskify.validation;
import com.iba.eTaskify.entity.OrgAdmin;
import com.iba.eTaskify.entity.RegistrationForm;
import com.iba.eTaskify.repository.RegistrationRepository;
import org.apache.commons.validator.GenericValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class RegistrationValidator implements Validator {

    @Autowired
    private RegistrationRepository registrationRepository;


    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(OrgAdmin.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        String regexNumbers = "^[0-9]*$";
        String alphaNumeric = "^[a-zA-Z0-9]+$";
        OrgAdmin admin = (OrgAdmin) o;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "organizationName", "Organization name can not be empty");
        if (!errors.hasFieldErrors("organizationName")) {
            if (!GenericValidator.isInRange(admin.getOrganizationName().length(), 3, 150)) {
                errors.rejectValue("organizationName", "Organization name length must be between 3 and 150");
            }else if (registrationRepository.existsByOrganizationName(admin.getOrganizationName())){
                errors.rejectValue("organizationName", "Organization has already registered");
            }
        }
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "phoneNumber", " Phone can not be empty");
        if (!errors.hasFieldErrors("phoneNumber")) {
            if (!GenericValidator.isInRange(admin.getPhoneNumber().length(), 7, 35)) {
                errors.rejectValue("phoneNumber", "Phone length must be between 7 and 35");
            } else if (!GenericValidator.matchRegexp(admin.getPhoneNumber(), regexNumbers)) {
                errors.rejectValue("phoneNumber", "Please enter only numbers");
            }
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "Email can not be empty");
        if (!errors.hasFieldErrors("email")) {
            if (!GenericValidator.isEmail(admin.getEmail())) {
                errors.rejectValue("email", "This is not an email");
            }else if (registrationRepository.existsByEmail(admin.getEmail())){
                errors.rejectValue("email", "Email has already registered");
            }
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "Password can not be empty");
        if (!errors.hasFieldErrors("password")) {
            if ((!GenericValidator.isInRange(admin.getPassword().length(), 6, 200))) {
                errors.rejectValue("password", "Password length must be between 8 and 150");
            }else if (!GenericValidator.matchRegexp(admin.getPhoneNumber(), alphaNumeric)) {
                errors.rejectValue("password", "Please enter only alphanumeric characters");
            }
        }
    }
}
