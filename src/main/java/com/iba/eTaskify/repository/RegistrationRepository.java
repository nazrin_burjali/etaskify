package com.iba.eTaskify.repository;

import com.iba.eTaskify.entity.RegistrationForm;
import com.iba.eTaskify.entity.Role;
import com.iba.eTaskify.entity.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RegistrationRepository extends CrudRepository<RegistrationForm, Integer> {
        boolean existsByOrganizationName(String organizationName);
        boolean existsByEmail(String email);
        List<RegistrationForm> findAll();
        Optional<RegistrationForm> findByEmail(String email);
        @Query(name = "findByUserId", value = "select role from registrationform where userid = :userid", nativeQuery = true)
        Role findByUserId(@Param(value = "userid") int userid);
         RegistrationForm findByUsername(String name);
         List<RegistrationForm> findByRole(String role);

}
