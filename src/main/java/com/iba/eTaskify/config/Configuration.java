package com.iba.eTaskify.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Data
public class Configuration {
    @Value("${jwt.token.secret}")
    private String jwtSecret;
}

