package com.iba.eTaskify.entity;

import lombok.Data;
import org.springframework.http.HttpStatus;

import java.io.Serializable;

import java.util.List;
@Data

    public class ApiError implements Serializable {

        private static final long serialVersionUID = 1L;

        private HttpStatus status;
        private String error;
        private Integer count;
        private List<String> errors;
    }