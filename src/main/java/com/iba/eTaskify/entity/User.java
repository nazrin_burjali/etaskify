package com.iba.eTaskify.entity;
import lombok.*;


@Data
public class User{
    private int userId;
    private String name;
    private String surname;
    private String email;
    private String password;
    private Role role;
}


