package com.iba.eTaskify.entity;

import lombok.Data;


@Data
public class OrgAdmin {
    private int id;
    private String organizationName;
    private String phoneNumber;
    private String address;
    private String username;
    private String email;
    private String password;
    private Role role;
}
