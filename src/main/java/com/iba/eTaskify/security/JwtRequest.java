package com.iba.eTaskify.security;

import lombok.Data;

@Data
public class JwtRequest {
    private String email;
    private String password;
}
