package com.iba.eTaskify.security;

import lombok.Data;


@Data
public class JwtResponse {
    private String token;
}
