package com.iba.eTaskify.security;

import com.iba.eTaskify.entity.RegistrationForm;
import com.iba.eTaskify.entity.Role;
import com.iba.eTaskify.repository.RegistrationRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.catalina.security.SecurityConfig;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Slf4j
@Service
public class eTaskifyUserDetailsService implements UserDetailsService {

    private RegistrationRepository registrationRepository;


    public eTaskifyUserDetailsService(RegistrationRepository registrationRepository) {
        this.registrationRepository = registrationRepository;
    }


    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        var optionalUser = registrationRepository.findByEmail(email).orElseThrow(() ->
        {throw new UsernameNotFoundException("User with email" + email + "not found");});


        UserPrincipal principal = new UserPrincipal(optionalUser);
        return principal;


        }
    }