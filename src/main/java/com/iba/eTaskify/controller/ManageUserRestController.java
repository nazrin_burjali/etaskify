package com.iba.eTaskify.controller;

import com.iba.eTaskify.dto.UserDto;
import com.iba.eTaskify.entity.RegistrationForm;
import com.iba.eTaskify.entity.User;
import com.iba.eTaskify.repository.RegistrationRepository;
import com.iba.eTaskify.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
//manageUser idi
@RestController
@RequestMapping("/admin/manageUser/")
public class ManageUserRestController {
//s
    @Autowired
    private UserService userService;

    @Autowired
    private RegistrationRepository registrationRepository;

    @PostMapping("/")
    public void saveUser(@RequestBody User user){
         userService.addUser(user);
    }

    @GetMapping("/")
    public List<RegistrationForm> getAll(){
        return registrationRepository.findAll();
    }

}
