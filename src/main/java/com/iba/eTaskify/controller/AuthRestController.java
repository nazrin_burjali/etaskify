package com.iba.eTaskify.controller;

import com.iba.eTaskify.security.JwtRequest;
import com.iba.eTaskify.security.JwtResponse;
import com.iba.eTaskify.security.UserPrincipal;
import com.iba.eTaskify.security.eTaskifyUserDetailsService;
import com.iba.eTaskify.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/")

public class AuthRestController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private eTaskifyUserDetailsService userDetailsService;



    @PostMapping("/auth")
    public ResponseEntity<JwtResponse> authenticate(@RequestBody JwtRequest request) {
        try {

            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(request.getEmail(), request.getPassword()));
            System.out.println(request.getEmail() + "DDDDDDD");
            //UserPrincipal userPrincipal = (UserPrincipal) userDetailsService.loadUserByUsername(request.getEmail());
            UserPrincipal userPrincipal = (UserPrincipal) userDetailsService.loadUserByUsername(request.getEmail());
            String token = jwtUtil.generateToken(userPrincipal);
            JwtResponse jwtResponse = new JwtResponse();
            jwtResponse.setToken(token);
            System.out.println("Testt");
            return ResponseEntity.ok(jwtResponse);



           // String token = jwtUtil.generateToken(userPrincipal);
//            JwtResponse jwtResponse = new JwtResponse();
   //         jwtResponse.setToken(token);



        } catch (BadCredentialsException e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

    }


}
