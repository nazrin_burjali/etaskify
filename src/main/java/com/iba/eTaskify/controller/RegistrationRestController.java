package com.iba.eTaskify.controller;
import com.iba.eTaskify.dto.RegistrationDto;
import com.iba.eTaskify.entity.OrgAdmin;
import com.iba.eTaskify.service.OrgAdminService;
import com.iba.eTaskify.validation.RegistrationValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
public class RegistrationRestController {

    @Autowired
    private OrgAdminService adminService;

    @Autowired
    private RegistrationValidator registrationValidator;


    @InitBinder
    public void initRegistrationBinder(WebDataBinder dataBinder) {
        if (dataBinder.getTarget() != null && (dataBinder.getTarget()).getClass() == OrgAdmin.class) {
            dataBinder.setValidator(registrationValidator);
        }
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/")
    public RegistrationDto register(@Validated @RequestBody OrgAdmin admin) {
        return adminService.addAdmin(admin);
    }

}

