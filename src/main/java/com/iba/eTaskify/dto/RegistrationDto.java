package com.iba.eTaskify.dto;

import com.iba.eTaskify.entity.Role;
import lombok.Data;


@Data
public class RegistrationDto {
    private String organizationName;
    private String phoneNumber;
    private String username;
    private String email;

}
