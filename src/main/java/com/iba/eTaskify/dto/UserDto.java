package com.iba.eTaskify.dto;

import com.iba.eTaskify.entity.Role;
import lombok.Data;

    @Data
    public class UserDto {
        private String name;
        private String surname;
        private String email;
    }

