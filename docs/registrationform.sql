--
-- PostgreSQL database dump
--

-- Dumped from database version 12.3
-- Dumped by pg_dump version 13.1

-- Started on 2021-03-02 01:14:45

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 219 (class 1259 OID 33299)
-- Name: registrationform; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.registrationform (
    id integer NOT NULL,
    address character varying(255),
    email character varying(255),
    name character varying(255),
    organization_name character varying(255),
    password character varying(255),
    phone_number character varying(255),
    role character varying(255),
    surname character varying(255),
    username character varying(255)
);


ALTER TABLE public.registrationform OWNER TO postgres;

--
-- TOC entry 218 (class 1259 OID 33297)
-- Name: registrationform_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.registrationform_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.registrationform_id_seq OWNER TO postgres;

--
-- TOC entry 2868 (class 0 OID 0)
-- Dependencies: 218
-- Name: registrationform_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.registrationform_id_seq OWNED BY public.registrationform.id;


--
-- TOC entry 2732 (class 2604 OID 33302)
-- Name: registrationform id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.registrationform ALTER COLUMN id SET DEFAULT nextval('public.registrationform_id_seq'::regclass);


--
-- TOC entry 2862 (class 0 OID 33299)
-- Dependencies: 219
-- Data for Name: registrationform; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.registrationform (id, address, email, name, organization_name, password, phone_number, role, surname, username) FROM stdin;
1	X.Mammadov 5	hjk@gmail.com	\N	Prohgjkgrgjhkess	$2a$10$6DjNZexiH/lLJbgvW2PGTugTIIM.PbkrOxFzFxtRQrXV1CL1r/TZ.	55426722090	\N	\N	admin
2	\N	test@gmail.com	\N	Test org	$2a$10$ZEkvKLvwHp9CI1oipvlvXOvTo1njMI5MBnLOsx/BZRl0bxz7/J5v6	55426722090	ADMIN	\N	admin_test
3	\N	testt@gmail.com	TestName	\N	$2a$10$Z2pjL.mw6j7uumYsRDGEZukqthyEkiHVwJhsePY7zCenIdkoonqVq	\N	USER	\N	\N
\.


--
-- TOC entry 2869 (class 0 OID 0)
-- Dependencies: 218
-- Name: registrationform_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.registrationform_id_seq', 3, true);


--
-- TOC entry 2734 (class 2606 OID 33307)
-- Name: registrationform registrationform_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.registrationform
    ADD CONSTRAINT registrationform_pkey PRIMARY KEY (id);


-- Completed on 2021-03-02 01:14:45

--
-- PostgreSQL database dump complete
--

